package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.repository.IUserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CUserController {
	
	@Autowired
	IUserRepository pUserRepository;
	
	@GetMapping("/users")
	public ResponseEntity<List<CUser>> getUser(){
		try {
			List<CUser> pUsers = new ArrayList<CUser>();
			
			pUserRepository.findAll().forEach(pUsers::add);
			
			return new ResponseEntity<>(pUsers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/user")
	public ResponseEntity<CUser> getUser(@RequestParam(value = "username") String username){
		try {			
			CUser cUser = pUserRepository.findByUsername(username);
			return new ResponseEntity<>(cUser, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/useremail")
	public ResponseEntity<CUser> getUserByEmail(@RequestParam(value = "email") String email){
		try {			
			CUser cUser = pUserRepository.findByEmail(email);
			return new ResponseEntity<>(cUser, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	// get by id
    @GetMapping("/user/{id}")
    public ResponseEntity<CUser> getCUserById(@PathVariable("id") long id) {
        // Todo: viết code lấy voucher theo id tại đây
        try {
            Optional<CUser> userData = pUserRepository.findById(id);
            if (userData.isPresent()) {
                return new ResponseEntity<>(userData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
	// creat new user
    @PostMapping("/users")
    public ResponseEntity<CUser> createCVoucher(@Valid @RequestBody CUser pUser) {
        try {
            // TODO: Hãy viết code tạo voucher đưa lên DB
           
            CUser _user = pUserRepository.save(pUser);
            return new ResponseEntity<>(_user, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	// update user
    @PutMapping("/user/{id}")
    public ResponseEntity<CUser> updateCVoucherById(@PathVariable("id") long id, @Valid @RequestBody CUser pUser) {
        try {
            
            Optional<CUser> userData = pUserRepository.findById(id);
            if (userData.isPresent()) {
                CUser user = userData.get();
                user.setFirstname(pUser.getFirstname());
                user.setEmail(pUser.getEmail());
                user.setUsername(pUser.getUsername());
                user.setLastname(pUser.getLastname());
				user.setCountry(pUser.getCountry());
				user.setSubject(pUser.getSubject());
				user.setCustomerType(pUser.getCustomerType());
				user.setRegisterStatus(pUser.getRegisterStatus());

                return new ResponseEntity<>(pUserRepository.save(user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
	// delete a user
    @DeleteMapping("/user/{id}")
    public ResponseEntity<CUser> deleteCVoucherById(@PathVariable("id") long id) {

        try {
            // TODO: Hãy viết code xóa 1 voucher từ DB
            pUserRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
